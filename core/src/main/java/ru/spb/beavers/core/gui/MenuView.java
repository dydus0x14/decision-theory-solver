package ru.spb.beavers.core.gui;

import ru.spb.beavers.core.data.StorageModules;
import ru.spb.beavers.modules.ITaskModule;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * View главного меню приложения
 */
public class MenuView extends JPanel {

    private final JPanel taskListPanel;

    public MenuView() {
        super(new GridBagLayout());
        GridBagConstraints constraints = new GridBagConstraints();
        constraints.gridx = 0;
        constraints.gridy = 0;
        constraints.weightx = 1.0;
        constraints.weighty = 0.2;
        constraints.gridwidth = 3;
        constraints.fill = GridBagConstraints.BOTH;
        constraints.anchor = GridBagConstraints.CENTER;
        this.add(new JLabel(), constraints);

        constraints.weighty = 0.2;
        constraints.gridy = 1;
        constraints.weightx = 0.2;
        constraints.gridwidth = 1;
        this.add(new JLabel(), constraints);

        constraints.gridx = 2;
        this.add(new JLabel(), constraints);

        taskListPanel = new JPanel(new GridBagLayout());
        JScrollPane scrollPane = new JScrollPane(taskListPanel);
        scrollPane.getVerticalScrollBar().setPreferredSize(new Dimension(0, 0));
        scrollPane.getVerticalScrollBar().setUnitIncrement(16);
        scrollPane.setOpaque(false);
        scrollPane.getViewport().setOpaque(false);
        constraints.gridx = 1;
        constraints.weighty = 0.8;
        this.add(scrollPane, constraints);

        constraints.gridy = 2;
        constraints.gridx = 0;
        constraints.weighty = 0.2;
        constraints.gridwidth = 3;
        this.add(new JLabel(), constraints);

        initMenu();
    }

    /**
     * Производит инициализацию главного меню
     */
    public void initMenu() {
        GridBagConstraints constraints = new GridBagConstraints();
        constraints.gridx = 0;
        constraints.gridy = GridBagConstraints.RELATIVE;
        constraints.insets = new Insets(3, 0, 0, 0);

        for (final ITaskModule module : StorageModules.getModules()) {
            JButton btnStartModule = new JButton(module.getTitle());
            btnStartModule.setPreferredSize(new Dimension(200, 30));
            btnStartModule.addActionListener(new ActionListener() {

                @Override
                public void actionPerformed(ActionEvent event) {
                    JPanel descriptionPanel = GUIManager.getDescriptionView().getDescriptionPanel();
                    descriptionPanel.removeAll();
                    module.initDescriptionPanel(descriptionPanel);

                    JPanel theoryPanel = GUIManager.getTheoryView().getTheoryPanel();
                    theoryPanel.removeAll();
                    module.initSolutionPanel(theoryPanel);

                    InputView inputView = GUIManager.getInputView();
                    inputView.initSaveButtonListener(module.getPressSaveListener());
                    inputView.initLoadButtonListener(module.getPressLoadListener());
                    inputView.initDefaultButtonListener(module.getDefaultValuesListener());
                    JPanel inputPanel = inputView.getInputPanel();
                    inputPanel.removeAll();
                    module.initInputPanel(inputPanel);

                    StorageModules.setActiveModule(module);
                    GUIManager.setActiveView(GUIManager.getDescriptionView());
                }
            });
            taskListPanel.add(btnStartModule, constraints);
        }
    }
}
