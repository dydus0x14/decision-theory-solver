package ru.spb.beavers.modules;

import edu.uci.ics.jung.algorithms.layout.FRLayout;
import edu.uci.ics.jung.graph.Graph;
import edu.uci.ics.jung.graph.SparseMultigraph;
import edu.uci.ics.jung.graph.util.EdgeType;
import edu.uci.ics.jung.visualization.GraphZoomScrollPane;
import edu.uci.ics.jung.visualization.VisualizationViewer;
import info.monitorenter.gui.chart.Chart2D;
import info.monitorenter.gui.chart.ITrace2D;
import info.monitorenter.gui.chart.traces.Trace2DSimple;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.*;
import java.util.Random;


public class TaskOfPaper implements ITaskModule {

    private class SaveListener implements ActionListener {
        public void actionPerformed(ActionEvent e)
        {
            System.out.println("SaveListener performed");

            //FileNameExtensionFilter filter = new FileNameExtensionFilter("*.*");
            JFileChooser fc = new JFileChooser();
            //fc.setFileFilter(filter);
            if ( fc.showSaveDialog(null) == JFileChooser.APPROVE_OPTION ) {
                try ( FileWriter fw = new FileWriter(fc.getSelectedFile()) ) {
                    fw.write(toFile());
                    fw.close();
                }
                catch ( IOException e1 ) {
                    System.out.println("Error of save\n" + e1.toString());
                }
            }

        }

        private String toFile() {
            String s = vcField.getText();
            s += "\n";
            s += vsField.getText();
            s += "\n";
            s += mField.getText();
            s += "\n";
            s += pField.getText();

            return s;
        }
    }

    private class LoadListener implements ActionListener {
        public void actionPerformed(ActionEvent e)
        {
            System.out.println("LoadListener performed");

            //FileNameExtensionFilter filter = new FileNameExtensionFilter("*.txt", "*.*");
            JFileChooser fc = new JFileChooser();
            //fc.setFileFilter(filter);
            if ( fc.showOpenDialog(null) == JFileChooser.APPROVE_OPTION ) {
                try ( FileReader fr = new FileReader(fc.getSelectedFile()))  {
                    BufferedReader br = new BufferedReader(fr);
                    vcField.setText(br.readLine());
                    vsField.setText(br.readLine());
                    mField.setText(br.readLine());
                    pField.setText(br.readLine());
                    fr.close();
                }
                catch ( IOException e1 ) {
                    System.out.println("Error of load\n" + e1.toString());
                }
            }
        }
    }

    private class DefaultValuesListener implements ActionListener {
        public void actionPerformed(ActionEvent e)
        {
            System.out.println("DefaultValuesListener performed");
            /*// Не работает
            vcField.setText("4");
            vsField.setText("6");
            mField.setText("2");
            pField.setText("0.2,0.5,0.3");*/
        }
    }

    private SaveListener saveListener = null;
    private LoadListener loadListener = null;
    private DefaultValuesListener defaultValuesListener = null;

    // Формат вывода дробных чисел
    DecimalFormat myFormatter = new DecimalFormat("###.###");

    /**
     * @return Название задачи. 1 - 4 слова.
     */
    public String getTitle() {
        return "2.1.2. Задача о газетах 11";
    }

    // Текстовое поле для краткого описания решаемой задачи
    private JTextArea descriptionTextAreaName = new JTextArea(1, 64);
    private JTextArea descriptionTextArea = new JTextArea(5, 70);
    //private JEditorPane descriptionTextArea = new JEditorPane();
    /**
     * Инициализация элементов на панели, дающей краткое описание решаемой задачи.
     * @param panel Инициализируемая панель.
     */
    public void initDescriptionPanel(JPanel panel) {
//        panel.setLayout(new GridBagLayout());
//        GridBagConstraints constraints = new GridBagConstraints();
//        constraints.gridx = GridBagConstraints.RELATIVE;
//        constraints.fill = GridBagConstraints.BOTH;
//
//        panel.add(new JLabel("LABEL"), constraints);
//
//        descriptionTextAreaName.setLineWrap(true);
//        descriptionTextAreaName.setWrapStyleWord(true);
//        Font font1 = new Font("Verdana", Font.BOLD, 12);
//        descriptionTextAreaName.setFont(font1);
//        descriptionTextAreaName.setText("\t\t\tЗадача о газетах");
//        panel.add(descriptionTextAreaName, constraints);
//
//        descriptionTextArea.setLineWrap(true);
//        descriptionTextArea.setWrapStyleWord(true);
//        //Font font2 = new Font("Verdana", Font.MONOSPACED, 12);
//        //descriptionTextArea.setFont(font2);
//        descriptionTextArea.setText("\n\tНеформальная постановка задачи:\n" +
//                "\n" +
//                "Продавец газет ежедневно закупает газеты по оптовой цене, а затем продает их по розничной. Не проданные в течение дня газеты полностью теряют свою стоимость. Спрос на газеты определяется различными внешними факторами и не носит регулярного характера. Доход продавца определяется количеством проданных газет и разницей между оптовой и розничной стоимостью газет. Если продавец закупает недостаточно газет и спрос на газеты превышает имеющееся количество газет то продавец не получит части дохода, которую мог бы получить, закупив большее число газет. Если продавец закупает слишком много газет и часть газет остается не проданными, то продавец теряет на каждой непроданной газете ее оптовую стоимость. Задача сводится к выбору количества газет, закупка и продажа которых принесут продавцу наибольший доход.\n");
//        panel.add(descriptionTextArea, constraints);

    }

    // Текстовое поле для подробного решения задачи
    //private JTextArea solutionTextArea = new JTextArea(1, 70);
    /**
     * Инициализация элементов на панели, предоставляющие подробное решение задачи.
     * Выводится полная теоритическая справка, вывод всех формул, графиков, описание входных/выходных данных задачи.
     * @param panel Инициализируемая панель.
     */
    public void initSolutionPanel(JPanel panel) {
//        //solutionTextArea.setLineWrap(true);
//        //solutionTextArea.setWrapStyleWord(true);
//        //solutionTextArea.setText("\tПодробное описание и решение задачи.");
//        //panel.add(solutionTextArea);
//
//        ImageIcon theory = new ImageIcon(TaskOfPaper.class.getResource("images/theory.jpg"));
//        JLabel imageLabel = new JLabel();
//        imageLabel.setIcon(new ImageIcon(theory.getImage().getScaledInstance(800, 2000, theory.getImage().SCALE_DEFAULT)));
//        panel.add(imageLabel);
    }

    // Элементы для ввода исходных данных
    private JTextField vcLabel = new JTextField();
    private JTextField vcField = new JTextField(12);
    private JTextField vsLabel = new JTextField();
    private JTextField vsField = new JTextField(12);
    private JTextField mLabel = new JTextField();
    private JTextField mField = new JTextField(12);
    private JTextField pLabel = new JTextField();
    private JTextField pField = new JTextField(30);
    // Текстовое поле для описания входных данных
    private JTextArea inputTextArea = new JTextArea(20, 70);
    /**
     * Инициализация элементов панели ввода исходных данных.
     * @param panel Инициализируемая панель.
     */
    public void initInputPanel(JPanel panel) {
//        vcLabel.setText("Стоимость запупки: ");
//        vcLabel.setEnabled(false);
//        vcLabel.setEditable(false);
//        vcField.setEditable(true);
//        vcField.setText("4");
//
//        vsLabel.setText("Стоимость продажи: ");
//        vsLabel.setEnabled(false);
//        vsLabel.setEditable(false);
//        vsField.setEditable(true);
//        vsField.setText("6");
//
//        mLabel.setText("Количество газет: ");
//        mLabel.setEnabled(false);
//        mLabel.setEditable(false);
//        mField.setEditable(true);
//        mField.setText("2");
//
//        pLabel.setText("Массив вероятностей: ");
//        pLabel.setEnabled(false);
//        pLabel.setEditable(false);
//        pField.setEditable(true);
//        pField.setText("0.2,0.5,0.3");
//
//        inputTextArea.setLineWrap(true);
//        inputTextArea.setWrapStyleWord(true);
//        inputTextArea.setText("\tОписание входных данных:\n" +
//                "Стоимость закупки - закупочная цена одной газеты.\n" +
//                "Стоимость продажи - отпускная цена одной газеты.\n" +
//                "Количество газет - максимальное количество газет, доступных для покупки предпринимателем.\n" +
//                "Массив вероятностей - массив, в котором указаны вероятности покупки каждого количества газет, начиная от нуля и заканчивая указанным выше количеством газет.\n" +
//                "Сумма всез вероятностей массива должна быть равна единице.\n\n" +
//                //"Тра-ля-ля, тра-ля-ля мы везем с собой кота,\n" +
//                //"Чижика, собаку, кошку, забияку ...\n\n" +
//                "\tОписание формата файла с исходными данными:\n" +
//                "I строка: <Цена оптовой закупки одной газеты>\n" +
//                "II строка: <Цена розничной продажи одной газеты>\n" +
//                "III строка: <Максимальное количество закупаемых газет>\n" +
//                "IV строка: <Массив вероятностей покупки каждого количества газет от 0 до максимального количества>\n\n" +
//                "\tПример формата файла с исходными данными:\n" +
//                "4\n" +
//                "6\n" +
//                "2\n" +
//                "0.2,0.3,0.5\n");
//
//        panel.add(vcLabel);
//        panel.add(vcField);
//        panel.add(vsLabel);
//        panel.add(vsField);
//        panel.add(mLabel);
//        panel.add(mField);
//        panel.add(pLabel);
//        panel.add(pField);
//        panel.add(inputTextArea);
    }

    // Текстовое поле для решения задачи
    private JTextArea outputTextArea = new JTextArea(2, 70);
    /**
     * Инициализация элементов панели оторбражающей решение задачи с введенными исходными данными.
     * @param panel Инициализируемая панель.
     */
    public void initExamplePanel(JPanel panel) {
        panel.add(outputTextArea);
        outputTextArea.setText("\t\tРешение, мать твою ...\n\n");

        // Вход
        double Vc; // Стоимость закупки
        double Vs; // Стоимость продажи
        int m; // Количество газет для покупки
        double[] p; // Массив вероятностей

        // Выход
        int d = 0; // Оптимальное количество газет
        double max = 0; // Максимальная теоретическая выручка
        double[] U; // Массив полезностей
        double[][] V; // Массив ценностей

        // Введем входные параметры
        try {
            Vc = Integer.parseInt(vcField.getText());
            Vs = Integer.parseInt(vsField.getText());
            m = Integer.parseInt(mField.getText());
        }
        catch (Exception e) {
            outputTextArea.append("\tВведены некоректные значения!!!\n");
            return;
        }

        //*********************************
        // Фокусы разработчиков
        // Для соответсвия количества газет (указали 5 газет, а получаем 4)
        m++;
        //*********************************

        p = new double [m];
        // Считывание введенных данных
        String[] items = pField.getText().split(",");
        for (int i = 0; i < items.length; i++) {
            try {
                p[i] = Double.parseDouble(items[i]);
            }
            catch (NumberFormatException nfe) {
                outputTextArea.append("\tВведены некоректные значения!!!\n");
                return;
            }
        }

        // Проверка значений вероятности
        double proverkaP = 0;
        for(int i = 0; i < m; i++) {
            proverkaP += p[i];
        }
        if (proverkaP != 1){
            outputTextArea.append("\tСумма массива вероятностей не равна 1!!!\n");
            return;
        }

        // Расчет полезностей
        U = new double [m];
        for(int i = 0; i < m; i++) {
            double Z = 0;
            for(int j = 0; j < i; j++) {
                Z += (i-j)*p[j];
            }
            U[i] = i*(Vs-Vc) - Vs*Z;
        }

        // Поиск максимального значения полезности и сохранения индекса.
        for(int i = 0; i < m; i++) {
            if(max < U[i]){
                max = U[i];
                d = i;
            }
        }

        // Расчет ценностей.
        V = new double [m][m];
        for(int i = 0; i < m; i++) {
            for(int j = 0; j < m; j++) {
                if (j < i){
                    V[j][i] = Vs * j - Vc * i;
                }
                else{
                    V[j][i] = Vs * i - Vc * i;
                }
            }
        }

        // Вывод на экран полученных значений.
        outputTextArea.append("\tМассив полученных полезностей:\n");
        for(int i = 0; i < m; i++) {
            outputTextArea.append(myFormatter.format(U[i]) + ";    ");
        }

        outputTextArea.append("\n\n" +
                "\tРезультат решения:\n" +
                "Максимальная полезность: " + myFormatter.format(max) + "\n");
        outputTextArea.append("Получена при покупке данного количества газет: " + d + "\n");

        outputTextArea.append("\nМассив полученных ценностей:\n");
        for(int i = 0; i < m; i++) {
            outputTextArea.append("\n");
            for(int j = 0; j < m; j++) {
                outputTextArea.append(myFormatter.format(V[j][i]) + "   ");
            }
        }

        // Дерево решений
        /*Graph<Integer, String> g = new UndirectedSparseGraph<Integer, String>();
        g.addVertex(1);
        g.addVertex(1);*/

        // Graph<V, E> where V is the type of the vertices
        // and E is the type of the edges
        Graph<Integer, String> g = new SparseMultigraph<Integer, String>();
        // Add some vertices. From above we defined these to be type Integer.
        g.addVertex((Integer)1);
        g.addVertex((Integer)2);
        g.addVertex((Integer)3);
        // Add some edges. From above we defined these to be of type String
        // Note that the default is for undirected edges.
        g.addEdge("Edge-A", 1, 2); // Note that Java 1.5 auto-boxes primitives
        g.addEdge("Edge-B", 2, 3);
        // Let's see what we have. Note the nice output from the
        // SparseMultigraph<V,E> toString() method
        System.out.println("The graph g = " + g.toString());
        // Note that we can use the same nodes and edges in two different graphs.
        Graph<Integer, String> g2 = new SparseMultigraph<Integer, String>();
        g2.addVertex((Integer)1);
        g2.addVertex((Integer)2);
        g2.addVertex((Integer)3);
        g2.addEdge("Edge-A", 1,3);
        g2.addEdge("Edge-B", 2,3, EdgeType.DIRECTED);
        g2.addEdge("Edge-C", 3, 2, EdgeType.DIRECTED);
        g2.addEdge("Edge-P", 2,3); // A parallel edge
        System.out.println("The graph g2 = " + g2.toString());

        // Вывод дерева решений
        FRLayout layout = new FRLayout<>(g2);
        //(/*(FRLayout<yournode, youredge>)*/layout).setMaxIterations(1000);
        //layout.setSize(new Dimension(700, 700));
        VisualizationViewer/*<yournode, youredge>*/ vv = new VisualizationViewer<>(layout);
        GraphZoomScrollPane scrollPane = new GraphZoomScrollPane(vv);
        //panel.add(scrollPane);

        //panel.add();


        // Графическое решение
        // Create a chart:
        Chart2D chart = new Chart2D();
        // Create an ITrace:
        ITrace2D trace = new Trace2DSimple();
        // Add the trace to the chart. This has to be done before adding points (deadlock prevention):
        chart.addTrace(trace);
        // Add all points, as it is static:
        Random random = new Random();
        for(int i=10000;i>=0;i--){
            trace.addPoint(i,random.nextDouble()*1000.0+i);
        }
        /*chart.getAxisX().setPaintGrid(true);
        chart.getAxisY().setPaintGrid(true);*/
        //chart.setMinimumSize(new Dimension(700, 700));
        //chart.setMaximumSize(new Dimension(700, 700));

        chart.setVisible(true);
        //this.setVisible(true);

        //panel.add(chart);
        //outputTextArea.append(chart.getSize().toString());


    }

    /**
     * Listener может бросать IllegalArgumentException с описанием ошибки.
     * @return Слушатель, который будет вызван для сохранения набора исходных данных в файл.
     */
    public ActionListener getPressSaveListener() throws IllegalArgumentException {
        if (saveListener == null) {
            saveListener = new SaveListener();
        }
        return saveListener;
    }

    /**
     * Listener может бросать IllegalArgumentException с описанием ошибки
     * @return Слушатель, который будет вызван для загрузки набора исходных данных из файла.
     */
    public ActionListener getPressLoadListener() throws IllegalArgumentException {
        if (loadListener == null) {
            loadListener = new LoadListener();
        }
        return loadListener;
    }

    /**
     * Listener может бросать IllegalArgumentException с описанием ошибки
     * @return Слушатель, который будет вызван для установки значений по умолчанию входных параметров задачи.
     */
    public ActionListener getDefaultValuesListener() throws IllegalArgumentException {
        if (defaultValuesListener == null) {
            defaultValuesListener = new DefaultValuesListener();
        }
        return defaultValuesListener;
    }
}